#include "nlauncherwindow.h"
#include "ui_nlauncherwindow.h"

NLauncherWindow::NLauncherWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::NLauncherWindow)
{
    ui->setupUi(this);
    //    this->showMaximized();
    this->showFullScreen();
    initWnd();

    systemTimer = new QTimer();
    connect(systemTimer,SIGNAL(timeout()),this,SLOT(slot_setSysTimeInfo()));
    systemTimer->start(1000);
    mettingTimer = new QTimer();
    connect(mettingTimer,SIGNAL(timeout()),this,SLOT(slot_setMettingTimeInfo()));
    mettingTimer->start(1000);
}

NLauncherWindow::~NLauncherWindow()
{
    delete ui;
}

void NLauncherWindow::initWnd()
{
    //初始化FloatBar
#if 1
    QDesktopWidget *desktop = QApplication::desktop();
    int width = desktop->width();
    int height = desktop->height();
#else
    int width = this->width();
    int height = this->height();
#endif
    qDebug()<<"width: "<<width<<"height: "<<height;
    mLeftFloatBar = new FloatBarWidget(this);
    mLeftFloatBar->setParentWinInfo(width,height);
    int left_x = 0;
    int left_y = height/2-mLeftFloatBar->height()/2;
    mLeftFloatBar->move(left_x,left_y);
    mLeftFloatBar->show();

    mRightFlotBar = new FloatBarWidget(this);
    mLeftFloatBar->setParentWinInfo(width,height);
    int right_x = width -mRightFlotBar->width();
    int right_y = height/2-mRightFlotBar->height()/2;
    mRightFlotBar->move(right_x,right_y);
    mRightFlotBar->show();

    ui->BottomFrame->setMaximumHeight(161);
    //初始化ListWidget
    QScrollBar *horzionalScrollBar=new QScrollBar(this);
    horzionalScrollBar->setStyleSheet(
                "QScrollBar:horizontal{background:rgb(144, 127, 72);padding:0px;border-radius:6px;max-height:12px;}"
                "QScrollBar:horizontal{background:rgb(255, 255, 255);padding:0px;border-radius:6px;max-height:12px;}"
                "QScrollBar::handle:horizontal{background:rgb(144,127,72);min-width:50px;border-radius:6px;}"
                "QScrollBar::handle:horizontal:hover{background:rgb(88,77, 44);}"
                "QScrollBar::handle:horizontal:pressed{background:rgb(90,80, 45);}"
                );
    ui->listWidget->setHorizontalScrollBar(horzionalScrollBar);
    ui->listWidget->setResizeMode(QListView::Adjust);   // 设置大小模式-可调节
    ui->listWidget->setViewMode(QListView::IconMode);   // 设置显示模式
    ui->listWidget->setMovement(QListView::Static);     // 设置单元项不可被拖动
    ui->listWidget->setIconSize(QSize(160,160));
    ui->listWidget->setFlow(QListView::LeftToRight);
    ui->listWidget->setWrapping(false);
    ui->listWidget->setFrameShape(QListWidget::NoFrame);
    ui->listWidget->setSpacing(0);
    ui->listWidget->setAutoScroll(true);
    ui->listWidget->setFocusPolicy(Qt::NoFocus);
    ui->listWidget->setStyleSheet(
                "QListWidget{background-color:transparent;padding-left:0px;padding-top:0px; padding-bottom:0px; padding-right:0px;}"
                "QListWidget::Item{padding-top:15px; padding-bottom:5px;border-image: url(:/launcher_res/icons/add_view_bg.png);; border-radius:1px;}"
                "QListWidget::Item:hover{background:rgb(145, 128, 70); }"
                "QListWidget::item:selected{color:white; border:2px solid white; border-style:solid;}"
                //                "QListWidget::item:selected:!active{border-width:2px; background-color:transparent }"
                );
    QFont font;
    font.setPixelSize(16);
    QStringList mIconList,mTextList;
    mIconList<<":/launcher_res/icons/window.png"<<":/launcher_res/icons/connection.png"<<":/launcher_res/icons/discussion.png"
            <<":/launcher_res/icons/gadget.png"<<":/launcher_res/icons/fileviewer.png"<<":/launcher_res/icons/browser.png"
           <<":/launcher_res/icons/gadget.png"<<":/launcher_res/icons/fileviewer.png"<<":/launcher_res/icons/browser.png";
    mTextList<<"Windows"<<"Connection"<<"Discussion"<<"Gadget"<<"File Viewer"<<"Browser"
            <<"Gadget"<<"File Viewer"<<"Browser";
    for(int i=0;i<mIconList.length();i++)
    {
        QListWidgetItem *pItem = new QListWidgetItem();
        pItem->setSizeHint(QSize(160,160));
        pItem->setTextColor(Qt::white);
        pItem->setTextAlignment(Qt::AlignVCenter|Qt::AlignHCenter);
        pItem->setFont(font);
        pItem->setIcon(QIcon(mIconList[i]));
        pItem->setText(mTextList[i]);
        ui->listWidget->addItem(pItem);
    }
    //初始化时间
    mMinute = 0;
    mSecond =0;
    slot_setSysTimeInfo();
    slot_setMettingTimeInfo();
}

void NLauncherWindow::slot_setMettingTimeInfo()
{
    QString Seconds,Minutes;
    if(mSecond>=59)
    {
        mSecond =0;
        mMinute +=1;
    }
    mSecond ++;
    if(mSecond>=0&&mSecond<=9)
    {
        Seconds = "0"+QString::number(mSecond);
    }
    else
    {
        Seconds = QString::number(mSecond);
    }
    if(mMinute>=0&&mMinute<=9)
    {
        Minutes = "0"+QString::number(mMinute);
    }
    else
    {
        Minutes = QString::number(mMinute);
    }
    QString MTimeLabel =Minutes+":"+Seconds;
    ui->MTimeLabel->setText(MTimeLabel);
}

void NLauncherWindow::slot_setSysTimeInfo()
{
#if 1
    const QString format = "ddd/d/MM/yyyy/hh/mm/ss";
    QString m_time   = QDateTime::currentDateTime().toString(format);
    m_week = m_time.section('/',0,0);
    m_day  = m_time.section('/',1,1);
    m_month = m_time.section('/',2,2);
    m_year = m_time.section('/',3,3);
    m_hour = m_time.section('/',4,4);
    m_minute = m_time.section('/',5,5);
    m_second = m_time.section('/',6,6);
#endif
    QString TimeLabel = m_hour+":"+m_minute;
    ui->TimeLabel->setText(TimeLabel);

    QString DateLabel = m_month+"-"+m_day;
    ui->DateLabel->setText(DateLabel);
    QString SecondLabel =":"+ m_second;
//    ui->SecondLabel->setText(SecondLabel);
    time_t c_time;
    struct tm*p;
    time(&c_time);
    p=localtime(&c_time);
    int Weekday = p->tm_wday;
    switch (Weekday) {
    case 0:
        ui->WeekDayLabel->setStyleSheet("border-image: url(:/launcher_res/icons/week_0.png);");
        break;
    case 1:
        ui->WeekDayLabel->setStyleSheet("border-image: url(:/launcher_res/icons/week_1.png);");
        break;
    case 2:
        ui->WeekDayLabel->setStyleSheet("border-image: url(:/launcher_res/icons/week_2.png);");
        break;
    case 3:
        ui->WeekDayLabel->setStyleSheet("border-image: url(:/launcher_res/icons/week_3.png);");
        break;
    case 4:
        ui->WeekDayLabel->setStyleSheet("border-image: url(:/launcher_res/icons/week_4.png);");
        break;
    case 05:
        ui->WeekDayLabel->setStyleSheet("border-image: url(:/launcher_res/icons/week_5.png);");
        break;
    case 6:
        ui->WeekDayLabel->setStyleSheet("border-image: url(:/launcher_res/icons/week_6.png);");
        break;
    default:
        ui->WeekDayLabel->setStyleSheet("border-image: url(:/launcher_res/icons/week_0.png);");
        break;
    }
}

void NLauncherWindow::on_endToolButton_clicked()
{
    this->mMinute = 0;
    this->mSecond = 0;
}

void NLauncherWindow::mouseDoubleClickEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton)
    {
        QMessageBox::StandardButton rb =QMessageBox::question(NULL,"QUESTION","Do you want to exit this window ？",QMessageBox::Yes|QMessageBox::No,QMessageBox::Yes);
        if(rb == QMessageBox::Yes)
        {
                    this->close();
        }
        else
        {
              ;
        }
    }
}
