#include "floatbarwidget.h"
#include "ui_floatbarwidget.h"

FloatBarWidget::FloatBarWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FloatBarWidget)
{
    ui->setupUi(this);
    isLeftBar = false;
    isRightBar = false;
    QDesktopWidget *desktop = QApplication::desktop();
    int width = desktop->width();
    int height = desktop->height();
    mParentWinHeight = height;
    mParentWinWidth = width;
}

FloatBarWidget::~FloatBarWidget()
{
    delete ui;
}

void FloatBarWidget::setLeftBarStatus(bool status)
{
    isLeftBar = status;
}

void FloatBarWidget::setRightBarStatus(bool status)
{
    isRightBar =status;
}

void FloatBarWidget::setParentWinInfo(int width, int height)
{
    this->mParentWinWidth = width;
    this->mParentWinHeight = height;
}

void FloatBarWidget::mousePressEvent(QMouseEvent *event)
{
    //    setLeftBarStatus(false);
    //    setRightBarStatus(false);
    if(this->pos().x()<this->mParentWinWidth/2)
    {
        setLeftBarStatus(true);
    }
    else if(this->pos().x()>mParentWinWidth/2)
    {
        setRightBarStatus(true);
    }
    this->windowPos = this->pos();
    this->mousePos = event->globalPos();
    this->dPos = mousePos - windowPos;
}

void FloatBarWidget::mouseReleaseEvent(QMouseEvent *event)
{

    if(this->pos().x()<mParentWinWidth/2)
    {
        setLeftBarStatus(false);
    }
    else if(this->pos().x()>mParentWinWidth/2)
    {
        setRightBarStatus(false);
    }
    Q_UNUSED(event);
}

void FloatBarWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
}

void FloatBarWidget::mouseMoveEvent(QMouseEvent *event)
{
    if(isLeftBar)
    {
        if(event->globalPos().y()-dPos.y()<0)
        {
            this->move(0,0);
        }
        else if(event->globalPos().y()-dPos.y()>=(mParentWinHeight-this->height()))
        {
            this->move(0,mParentWinHeight-this->height());
        }
        else
        {
            this->move(0,event->globalPos().y()-dPos.y());//
        }
    }
    else if (isRightBar)
    {
        if(event->globalPos().y()-dPos.y()<0)
        {
            this->move(this->mParentWinWidth-this->width(),0);
        }
        else if(event->globalPos().y()-dPos.y()>=(mParentWinHeight-this->height()))
        {
            this->move(this->mParentWinWidth-this->width(),mParentWinHeight-this->height());
        }
        else
        {
            this->move(this->mParentWinWidth-this->width(),event->globalPos().y()-dPos.y());
        }
    }
}
