#ifndef FLOATBARWIDGET_H
#define FLOATBARWIDGET_H

#include <QWidget>
#include <QDebug>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QMouseEvent>
#include <QMoveEvent>

namespace Ui {
class FloatBarWidget;
}

class FloatBarWidget : public QWidget
{
    Q_OBJECT

public:
    explicit FloatBarWidget(QWidget *parent = 0);
    ~FloatBarWidget();

    void setLeftBarStatus(bool status);
    void setRightBarStatus(bool status);

    void setParentWinInfo(int width,int height);
protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseDoubleClickEvent(QMouseEvent *event);

private:
    Ui::FloatBarWidget *ui;
    QPoint windowPos;
    QPoint mousePos;
    QPoint dPos;
    bool isLeftBar,isRightBar;
    int mParentWinWidth,mParentWinHeight;
};

#endif // FLOATBARWIDGET_H
