#ifndef NLAUNCHERWINDOW_H
#define NLAUNCHERWINDOW_H

#include <QWidget>
#include <QDebug>
#include <QTime>
#include <QTimer>
#include <QDateTime>
#include <QScrollBar>
#include <QMessageBox>
#include <QDesktopServices>
#include <QDesktopWidget>
#include <QMouseEvent>
#include <QMoveEvent>
#include "time.h"
#include "floatbarwidget.h"

namespace Ui {
class NLauncherWindow;
}

class NLauncherWindow : public QWidget
{
    Q_OBJECT

public:
    explicit NLauncherWindow(QWidget *parent = 0);
    ~NLauncherWindow();
    void initWnd();

public slots:
    void slot_setSysTimeInfo();
    void slot_setMettingTimeInfo();

private slots:
    void on_endToolButton_clicked();

protected:
    void mouseDoubleClickEvent(QMouseEvent *event);
private:
    Ui::NLauncherWindow *ui;
    FloatBarWidget *mLeftFloatBar,*mRightFlotBar;
    QString m_year,m_month,m_day,m_week,
    m_hour,m_minute,m_second;
    QTimer *systemTimer,*mettingTimer;
    int mMinute,mSecond;
};

#endif // NLAUNCHERWINDOW_H
